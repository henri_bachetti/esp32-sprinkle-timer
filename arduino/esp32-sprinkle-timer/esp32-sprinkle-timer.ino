
#include <WiFi.h>
#include <esp_wifi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <SPIFFS.h>
#include <sntp.h>

#include "config.h"
#include "schedule.h"
#include "watering.h"
#include "flow.h"
#include "humidity.h"
#include "html.h"
#include "hmi.h"
#include "oled.h"

const char *ssid;
const char *password;

AsyncWebServer server(80);

Config config("/config.ini");
Schedule schedule(SCHEDULE_FILE);

const char* ntpServer = "pool.ntp.org";

Hmi hmi;

void handleNotFound(AsyncWebServerRequest *request) {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += request->url();
  message += "\nMethod: ";
  message += request->methodToString();
  message += "\nArguments: ";
  message += request->args();
  message += "\n";
  for (uint8_t i = 0; i < request->args(); i++) {
    message += " " + request->argName(i) + ": " + request->arg(i) + "\n";
  }
  request->send(404, "text/plain", message);
}

void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  Serial.println("Connected to AP successfully!");
}

void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info) {
  Serial.printf("WiFi connected to %s\n", ssid);
  Serial.print("IP address: "); Serial.println(WiFi.localIP());
}

void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(info.disconnected.reason);
  Serial.printf("Trying to reconnect to %s\n", ssid);
  WiFi.reconnect();
}

void setup(void)
{
  Serial.begin(115200);
  if (!display.begin()) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  hmi.begin();
  if (!SPIFFS.begin()) {
    Serial.println("SPIFFS.begin() failed");
  }
  if (config.read() != true) {
    Serial.println("configuration failed");
    while (1) sleep(1);
  }
  config.print();
  if (schedule.read() != true) {
    Serial.println("schediule configuration failed");
    while (1) sleep(1);
  }
  schedule.print();
  WiFi.onEvent(WiFiStationConnected, SYSTEM_EVENT_STA_CONNECTED);
  WiFi.onEvent(WiFiGotIP, SYSTEM_EVENT_STA_GOT_IP);
  WiFi.onEvent(WiFiStationDisconnected, SYSTEM_EVENT_STA_DISCONNECTED);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  ssid = config.getSsid();
  password = config.getPassword();
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(ssid);

  //  File file = SPIFFS.open("/index.html");
  //  if (!file || file.isDirectory()) {
  //    Serial.println("− failed to open file for reading");
  //    return;
  //  }
  //  Serial.println("− read from file:");
  //  while (file.available()) {
  //    Serial.write(file.read());
  //  }
  //  file.close();

  server.on("/", handleRoot);

  server.on("/configure", handleConfigure);

  server.on("/configure_submit", handleConfigureSubmit);

  server.on("/new", handleNew);

  server.on("/new_submit", handleNewSubmit);

  server.on("/manual", handleManual);

  server.on("/manual_in_progress", handleManualInProgress);

  server.on("/maint.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/maint.html", "text/html", false);
  });

  server.on("/test_relays", HTTP_GET, [](AsyncWebServerRequest * request) {
    Serial.println("Relays test");
    handleTest(request);
  });

  server.on("/on", HTTP_GET, [](AsyncWebServerRequest * request) {
    if (request->hasParam("relay")) {
      String name = request->arg("relay");
      Serial.printf("ON relay=%s\n", name.c_str());
      Relay *relay = Relay::getByName(name.c_str());
      if (relay == 0) {
        Serial.printf("%s: not found\n", name.c_str());
      }
      else {
        relay->on();
      }
    }
    handleTest(request);
  });

  server.on("/off", HTTP_GET, [](AsyncWebServerRequest * request) {
    if (request->hasParam("relay")) {
      String name = request->arg("relay");
      Serial.printf("ON relay=%s\n", name.c_str());
      Relay *relay = Relay::getByName(name.c_str());
      if (relay == 0) {
        Serial.printf("%s: not found\n", name.c_str());
      }
      else {
        relay->off();
      }
    }
    handleTest(request);
  });

  server.on("/config_ini", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/config.ini", "text/plain", false);
  });

  server.on("/schedule_ini", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/schedule.ini", "text/plain", false);
  });

  server.onNotFound(handleNotFound);

  server.begin();
  //  esp_wifi_set_ps(WIFI_PS_MIN_MODEM);
  Serial.println("HTTP server started");

  configTzTime("CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00", ntpServer);

  Serial.println("RESET all relays");
  Relay *relay = Relay::getFirst();
  while (relay != 0) {
    if (relay->isPresent()) {
      relay->off();
    }
    relay = Relay::getNext();
  }
  flowInit();
}

bool sntp_sync;

void loop(void)
{
  static time_t lastSec, lastFlow;
  time_t timestamp = time(NULL);
  float flow;

  if (sntp_get_sync_status() == SNTP_SYNC_STATUS_COMPLETED) {
    sntp_sync = true;
  }
  if (sntp_sync) {
    if (timestamp > 10) {
      if (timestamp - lastFlow >= 1) {
        lastFlow = timestamp;
        flow = getFlow();
        if (!hmi.isBusy()) {
          display.displayFlow(flow);
        }
        if (flow > Config::getConfig()->getMaxFlow()) {
          Serial.printf("flow is too high\n");
          Watering::stopAllAutoWatering();
          Way::stopAllManualWatering();
        }
      }
      if (timestamp - lastSec >= 60) {
        int moisture;
        getSoilMoisture(&moisture); // just display to terminal
        if (!hmi.isBusy()) {
          display.displayMoisture(moisture);
        }
        display.displayTimeDate();
        lastSec = timestamp;
        Watering::run(timestamp);
      }
    }
    hmi.run();
  }
}
