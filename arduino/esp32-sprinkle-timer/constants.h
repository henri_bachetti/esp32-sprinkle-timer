
// max line length in config file
#define MAX_LINE        250
// max definition length for object names (relays, ways, zones, etc.)
#define MAX_DEF         50
#define MAX_BUF         30

// max ways and relays number
#define MAX_WAY         128
// max zones number
#define MAX_ZONE        12
// max waterings number in a day for each way
#define MAX_SCHEDULE    4
// max waterings number in a day for all ways
#define MAX_WATERING    256
