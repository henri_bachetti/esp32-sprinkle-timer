# ESP32 SPRINKLE TIMER

The purpose of this page is to explain step by step the realization of an ESP32 sprinkle timer.

The board uses the following components :

 * an ESP32 board (DEV-KIT-C for example)
 * an OLED 128x64 SSD1306 display
 * an MP1584 STEP-DOWN module
 * 2 LEDs
 * 2 TACT boutons
 * a flow sensor
 * a moisture sensor
 * some resistors
 * the board is powered by a 12V or 24V power supply

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.11.

This first version is not fully operational. Only the relays configuration is implemented.

### BLOG
A description in french here : 

